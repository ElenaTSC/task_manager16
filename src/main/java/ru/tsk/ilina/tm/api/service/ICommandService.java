package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
