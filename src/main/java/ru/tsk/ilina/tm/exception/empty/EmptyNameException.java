package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty");
    }

    public EmptyNameException(String message) {
        super("Error! " + message + " name is empty");
    }

}
