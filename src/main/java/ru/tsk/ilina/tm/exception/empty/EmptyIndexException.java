package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error! Index is empty");
    }

    public EmptyIndexException(String message) {
        super("Error! " + message + " index is empty");
    }

}
